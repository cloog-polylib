/* Generated from ../../../git/cloog-polylib/cloog-core/test/union.cloog by CLooG 0.16.0-3-g43f556e gmp bits. */
if (M >= 11) {
  for (c1=-100;c1<=0;c1++) {
    S1(-c1);
  }
}
if (M <= -1) {
  for (c1=0;c1<=100;c1++) {
    S1(c1);
  }
}
if ((M >= 1) && (M <= 10)) {
  for (c1=0;c1<=100;c1++) {
    S1(c1);
  }
}
