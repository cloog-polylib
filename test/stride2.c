/* Generated from /home/skimo/git/cloog/test/stride2.cloog by CLooG 0.14.0-277-g4be103b gmp bits. */
for (c1=3;c1<=100;c1+=3) {
  for (c2=max(ceild(-c1+27,24),ceild(100*c1-2700,219));c2<=floord(c1,3);c2++) {
    if ((c1 == 27) && (c2 == 0)) {
      S1(27);
    }
    if (c1 == 3*c2) {
      S2(c1,c1/3);
    }
  }
}
