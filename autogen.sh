#!/bin/sh
test -d autoconf || mkdir autoconf
libtoolize -c --force
aclocal -I m4
automake -a -c --foreign
autoconf
if test -f cloog-core/autogen.sh; then
	(cd cloog-core; ./autogen.sh)
fi
